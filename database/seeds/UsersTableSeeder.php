<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [    
                [
                'name' => 'a',
                'email' => 'a@a.com',
                'role' => 1,
                'password'=> Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'credit'=> 23000,
                ],
                [
                'name' => 'b',
                'email' => 'b@b.com',
                'role' => 0,
                'password'=> Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'credit'=> 23000,
                ],
                [
                'name' => 'c',
                'email' => 'c@c.com',
                'role' => 1,
                'password'=> Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'credit'=> 14000,
                ],
                    
            ]
        );
    }
}