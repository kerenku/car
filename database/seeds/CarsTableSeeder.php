<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert(
            [    
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 1,
                'brand' => 'Mazda',
                'price'=> 10000,
                'status'=>0,
                'contact'=>'111'
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 1,
                'brand' => 'Ford',
                'price'=> 20000,
                'status'=>0,
                'contact'=>'111',
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 2,
                'brand' => 'Toyota',
                'price'=> 30000,
                'status'=>0,
                'contact'=>'222',
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 2,
                'brand' => 'BMW',
                'price'=> 40000,
                'status'=>0,
                'contact'=>'222',
                ],
                
            ]
            );
    }
}