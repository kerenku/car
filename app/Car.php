<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable=[
        'brand',
        'status',
        'price',
        'contact',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
