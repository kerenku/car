@extends('layouts.app')
@section('content')
<h1>Put your car up for sale</h1>
<form method = 'POST' action ="{{action('CarController@store')}}">
@csrf
<div class = "form-group">
<label for = "brand">Brand</label>
<input type = "text" class= "form-control" name = "brand">
<label for = "price">Price</label>
<input type = "number" class= "form-control" name = "price">
<label for = "contact">Contact Info</label>
<input type = "number" class= "form-control" name = "contact">
</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>

<!-- add following if asked for verification -->
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


</form>
@endsection